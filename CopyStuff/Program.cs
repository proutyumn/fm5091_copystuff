﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopyStuff
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine("copying data using classes...");
            TestClasses();
            Console.Clear();
            Console.WriteLine("copying data using structs...");
            TestStructs();

        }

        static void TestClasses()
        {
            NewClassData[] d1 = new NewClassData[3];
            NewClassData a = new NewClassData()
            {
                AnInteger = 3
            };
            NewClassData b = new NewClassData()
            {
                AnInteger = 32
            };
            NewClassData c = new NewClassData()
            {
                AnInteger = 90
            };
            d1[0] = a;
            d1[1] = b;
            d1[2] = c;

            NewClassData[] d2 = d1;
            Console.WriteLine("setting d2 = d1...");
            Console.WriteLine(d2[1].AnInteger);
            d1[1].AnInteger = 500;
            Console.WriteLine(d2[1].AnInteger);
            d1[1].AnInteger = 32;

            d2 = new NewClassData[3];
            d1.CopyTo(d2, 0);
            Console.WriteLine("using the .CopyTo method...");
            Console.WriteLine(d2[1].AnInteger);
            d1[1].AnInteger = 500;
            Console.WriteLine(d2[1].AnInteger);
            d1[1].AnInteger = 32;

            d2 = new NewClassData[3];
            for (int i = 0; i < 3; i++)
            {
                d2[i] = d1[i];
            }
            Console.WriteLine("using a for loop...");
            Console.WriteLine(d2[1].AnInteger);
            d1[1].AnInteger = 500;
            Console.WriteLine(d2[1].AnInteger);
            d1[1].AnInteger = 32;

            d2 = new NewClassData[3];
            d2[1] = new NewClassData()
            {
                AnInteger = d1[1].AnInteger
            };
            Console.WriteLine("explicitly creating a new object and copying members...");
            Console.WriteLine(d2[1].AnInteger);
            d1[1].AnInteger = 500;
            Console.WriteLine(d2[1].AnInteger);
            d1[1].AnInteger = 32;

            Console.ReadLine();
        }

        static void TestStructs()
        {
            NewStructData[] d1 = new NewStructData[3];
            NewStructData a = new NewStructData()
            {
                AnInteger = 3
            };
            NewStructData b = new NewStructData()
            {
                AnInteger = 32
            };
            NewStructData c = new NewStructData()
            {
                AnInteger = 90
            };
            d1[0] = a;
            d1[1] = b;
            d1[2] = c;

            NewStructData[] d2 = d1;
            Console.WriteLine("setting d2 = d1...");
            Console.WriteLine(d2[1].AnInteger);
            d1[1].AnInteger = 500;
            Console.WriteLine(d2[1].AnInteger);
            d1[1].AnInteger = 32;

            d2 = new NewStructData[3];
            d1.CopyTo(d2, 0);
            Console.WriteLine("using the .CopyTo method...");
            Console.WriteLine(d2[1].AnInteger);
            d1[1].AnInteger = 500;
            Console.WriteLine(d2[1].AnInteger);
            d1[1].AnInteger = 32;

            d2 = new NewStructData[3];
            for (int i = 0; i < 3; i++)
            {
                d2[i] = d1[i];
            }
            Console.WriteLine("using a for loop...");
            Console.WriteLine(d2[1].AnInteger);
            d1[1].AnInteger = 500;
            Console.WriteLine(d2[1].AnInteger);
            d1[1].AnInteger = 32;

            d2 = new NewStructData[3];
            d2[1] = new NewStructData()
            {
                AnInteger = d1[1].AnInteger
            };
            Console.WriteLine("explicitly creating a new object and copying members...");
            Console.WriteLine(d2[1].AnInteger);
            d1[1].AnInteger = 500;
            Console.WriteLine(d2[1].AnInteger);
            d1[1].AnInteger = 32;

            Console.ReadLine();
        }
    }

    class NewClassData
    {
        public int AnInteger { get; set; }
    }

    struct NewStructData
    {
        public int AnInteger { get; set; }
    }

}
